#pragma once
#include "CanConnectionBase.h"
#include "PCANBasic.h"
#include <Windows.h>
#include <thread>
#include <atomic>

class PcanConnection :
	public CanConnectionBase
{
public:
	PcanConnection();
	~PcanConnection();

	// Geerbt �ber CanConnectionBase
	virtual fmi2Status Initialize() override;
	virtual fmi2Status Uninitialize() override;
	virtual fmi2Status Send(unsigned long id, std::vector<unsigned char> data) override;
private:
	// Array of possible channel handles
	static const TPCANHandle CAN_Handles[3];
	// The windows event for receiving values
	HANDLE _receiveEvent;
	// The channel that is used
	WORD _channel;
	// Thread for receiving event based
	std::thread _receiverThread;
	// Set true and stop the receiver loop
	std::atomic_bool _stop;

	// Converts the PCanStatus to an appropiate fmi2Status
	fmi2Status checkStatus(TPCANStatus status);
	// This method is used for Event Based receiving
	void startReceive();
};

