#include "PcanConnection.h"
#include <string>

const TPCANHandle PcanConnection::CAN_Handles[] = { PCAN_USBBUS1, PCAN_USBBUS2, PCAN_PCIBUS1 };

PcanConnection::PcanConnection()
{
	_stop.store(false);
}

PcanConnection::~PcanConnection()
{
}

fmi2Status PcanConnection::Initialize()
{
	_receiveEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	// Init the CAN
	// Initialize the CAN form the possible devices list
	for (TPCANHandle handle : CAN_Handles)
	{
		TPCANStatus initRes = CAN_Initialize(handle, PCAN_BAUD_1M);
		if (initRes == PCAN_ERROR_OK)
		{
			// Success
			_channel = handle;
			// Automatically reset bus on error
			TPCANParameter autoResetValue = PCAN_PARAMETER_ON;
			CAN_SetValue(_channel, PCAN_BUSOFF_AUTORESET, &autoResetValue, sizeof(autoResetValue));
			// Log the great news
			std::string msg = "Initialized CAN_Channel: ";
			msg.append(std::to_string(handle));
			onLog(fmi2OK, msg.c_str());
			// Begin receiving
			_receiverThread = std::thread(std::bind(&PcanConnection::startReceive, this));
			return fmi2OK;
		}
	}
	// No valid handle found
	onLog(fmi2Error, "Could not connect to the CAN interface.");
	return fmi2Error;
}

fmi2Status PcanConnection::Uninitialize()
{
	// Stop reading
	_stop.store(true);
	if (_receiverThread.joinable())
	{
		_receiverThread.join();
	}
	CloseHandle(_receiveEvent);
	// Free the CanBus
	CAN_Uninitialize(_channel);
	return fmi2OK;
}

fmi2Status PcanConnection::Send(unsigned long id, std::vector<unsigned char> data)
{
	TPCANMsg msg = {};
	if (data.size() > sizeof(msg.DATA))
	{
		onLog(fmi2Error, "The data is too large for a CAN message!");
		return fmi2Error;
	}
	memcpy(msg.DATA, data.data(), data.size());
	msg.ID = id;
	msg.LEN = static_cast<BYTE>(data.size());
	msg.MSGTYPE = PCAN_MESSAGE_STANDARD;
	return checkStatus(CAN_Write(_channel, &msg));
}

fmi2Status PcanConnection::checkStatus(TPCANStatus status)
{
	// OK
	if (status == PCAN_ERROR_OK)
	{
		return fmi2OK;
	}

	// An Error
	if (status == PCAN_ERROR_BUSOFF)
	{

		// About the time a reset needs
		Sleep(500);
	}

	// Logs the whole error message for any error
	char strMsg[256];
	CAN_GetErrorText(status, 0, strMsg);
	onLog(fmi2Error, strMsg);
	return fmi2Error;
}
void PcanConnection::startReceive()
{
	// Use _receiveEvent for the CAN_Receive
	if (fmi2OK != checkStatus(CAN_SetValue(_channel, PCAN_RECEIVE_EVENT, &_receiveEvent, sizeof(_receiveEvent))))
	{
		return;
	}

	// Receiver loop
	while (!_stop.load())
	{
		//Wait for CAN Data and only use as much processor as neccessary
		DWORD waitRes = WaitForSingleObject(_receiveEvent, 100);

		// Signaled state :-)
		if (waitRes == WAIT_OBJECT_0)
		{
			// Always read the whole queue!
			TPCANMsg msg;
			TPCANStatus status;
			do
			{
				// Read without timestamp
				status = CAN_Read(_channel, &msg, NULL);

				// Received something
				if (status != PCAN_ERROR_QRCVEMPTY)
				{
					// Only process standard frames
					if (msg.MSGTYPE == PCAN_MESSAGE_STANDARD)
					{
						std::vector<unsigned char> data(msg.LEN);
						memcpy(data.data(), msg.DATA, data.size());
						onRead(msg.ID, data);
					}
				}
				else
				{
					// Error occured
					checkStatus(status);
				}
			} while (!_stop.load() && (status & PCAN_ERROR_QRCVEMPTY) != PCAN_ERROR_QRCVEMPTY);
		}
		else if (waitRes == WAIT_FAILED)
		{
			// Get the system error
			char buf[256];
			FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
						  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), buf, 256, NULL);
			onLog(fmi2Warning, buf);
		}
	}
	// Reset the stop trigger
	_stop.store(false);

	// Resets the Event-handle configuration, by setting a nullpointer handle
	DWORD dwTemp = 0;
	CAN_SetValue(_channel, PCAN_RECEIVE_EVENT, &dwTemp, sizeof(dwTemp));
}
