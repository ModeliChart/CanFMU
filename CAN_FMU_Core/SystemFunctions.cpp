#include "SystemFunctions.h"
#include "CanConnectionBase.h"

#if defined (_WIN32) && ! defined (UNIT_TEST)
#include "../WindowsLib/PcanConnection.h"
#elif __linux__
#include "../SocketCAN_FMU/SocketCanConnection.h"
#endif

std::unique_ptr<CanConnectionBase> GetSystemCan()
{
#if defined (_WIN32) && ! defined (UNIT_TEST)
	return std::make_unique<PcanConnection>();
#elif __linux__
	return std::unique_ptr<SocketCanConnection>(new SocketCanConnection());
#endif
	return nullptr;
}
