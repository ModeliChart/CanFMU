#pragma once
#include "TypeDefs.h"
#include <memory>
#include <vector>

class CanMessage;

class ConfigParser
{
public:
	ConfigParser(LoggerCallback logger);
	~ConfigParser();

	fmi2Status LoadConfig(
		const std::string &resUri	///< URI to the resource dir according the fmi standard.
	);

	std::string GetGuid();
	std::vector<std::shared_ptr<CanMessage>> GetCanMessages();

private:
	LoggerCallback _logger;
	std::string _guid;
	std::vector<std::shared_ptr<CanMessage>> _canMessages;
};

