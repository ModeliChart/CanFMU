#pragma once
// This header includes typedefs that are from global interest
// Circular references can be avoided
#include "fmi2FunctionTypes.h"
#include <functional>

typedef std::function<void(fmi2Status status, std::string message)> LoggerCallback;