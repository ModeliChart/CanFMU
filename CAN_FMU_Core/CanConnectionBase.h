#pragma once
#include "fmi2FunctionTypes.h"
#include "TypeDefs.h"
#include <vector>
#include <array>
#include <functional>

class CanMessage;

typedef std::function<void(unsigned long id, const std::vector<unsigned char> data)> ReadCallback;

/// This interface describes what a CAN Handle should be able to execute
class CanConnectionBase
{
public:
	void SetReadCallback(ReadCallback callback);
	void SetLoggerCallback(LoggerCallback logger);
	virtual fmi2Status Initialize() = 0;
	virtual fmi2Status Uninitialize() = 0;
	virtual fmi2Status Send(unsigned long id, std::vector<unsigned char> data) = 0;
protected:
	void onRead(unsigned long id, std::vector<unsigned char> data);
	void onLog(fmi2Status status, std::string msg);
private:
	ReadCallback _readCallback;
	LoggerCallback _logger;
};