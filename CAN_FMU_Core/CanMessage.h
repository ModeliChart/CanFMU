#pragma once
#include "TypeDefs.h"
#include "CanValueConverter.h"
#include <array>
#include <atomic>
#include <mutex>
#include <vector>

/// Enables translating the data of the CAN Frames.
/// Can be configured for different resolutions and scalings.
/// Data access is not threadsafe!
class CanMessage
{
public:
	CanMessage();
	CanMessage(LoggerCallback logger, bool isSender, unsigned long msgId);
	~CanMessage();

	/// Returns the message ID.
	unsigned long GetId();
	/// Is this a message to be sent (settable)
	bool IsSender();
	/// Returns the value 
	double GetValue(int pos);
	/// Returns the decoded values.
	std::vector<double> GetValues();
	/// Returns the encoded Data.
	std::vector<unsigned char> GetData();
	/// Is this a sender message?
	void SetIsSender(bool isSender);
	/// Set the ID.
	void SetMsgId(uint32_t id);
	/// Set the logger callback.
	void SetLoggerCallback(LoggerCallback logger);
	/// Set the description of the frame
	void SetFrame(
		int resolution,	///< How many Bits describe one value?
		std::vector<CanValueConverter>	///< The conversion Analog & Digital
	);
	/// Set one value
	void SetValue(int pos, double value);
	/// Set and encode the values.
	void SetValues(std::vector<double> values);
	/// Set the whole data as encoded CanData
	void SetData(std::vector<unsigned char> data);

private:
	// Maximum of 8 bytes
	const int MAX_MESSAGE_SIZE = 8;
	// Reserve (timecode)
	const int RESERVE_BITS_COUNT = 4;

	// Are we a sender?
	bool _isSender;
	// ID to sned to or receive from
	uint32_t _id;
	// Convert the data
	std::vector<CanValueConverter> _valueConverters;
	// CAN message is max 8 Bytes. Atomic for high frequent updates
	std::atomic<uint64_t> _data;
	// The size of the data in bytes
	size_t _dataSize = 0;

	// Callback for logging
	LoggerCallback _logger;

	// Resolution in Bits, e.g. 8Bit
	int _resolution;


	// Extract the encoded values from the data
	std::vector<uint64_t> getEncodedValuesTim();
	// Extract the encoded values from the data
	std::vector<uint64_t> getEncodedValues();
	// Move the encoded values to the data
	void setEncodedValuesTim(std::vector<uint64_t> encodedValues);
	// Move the encoded values to the data Jonas Style
	void setEncodedValues(std::vector<uint64_t> encodedValues);
};

