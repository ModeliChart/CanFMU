#include "CanMessage.h"
#include <algorithm>
#include <cstring>
#include <string>

CanMessage::CanMessage()
{
}

CanMessage::CanMessage(LoggerCallback logger, bool isSender, unsigned long msgId) :
	_logger(logger), _data(0)
{
	// Init data empty
	_isSender = isSender;
	SetMsgId(msgId);
}

CanMessage::~CanMessage()
{
}

unsigned long CanMessage::GetId()
{
	return _id;
}

bool CanMessage::IsSender()
{
	return _isSender;
}

double CanMessage::GetValue(int pos)
{
	if (pos < _valueConverters.size())
	{
		// Decode only one value
		return _valueConverters[pos].DecodeValue(getEncodedValues()[pos]);
	}
	return 0;
}

std::vector<double> CanMessage::GetValues()
{
	// Create the result vector
	std::vector<double> result;
	// Get the values
	auto values = getEncodedValues();

	if (values.size() > _valueConverters.size())
	{
		_logger(fmi2Warning, "Trying to set more values than defined in the config.");
		result.resize(_valueConverters.size());
		return result;
	}

	// Iterate over encoded values
	for (auto i = 0; i < values.size(); i++)
	{

		result.push_back(_valueConverters[i].DecodeValue(values[i]));
	}
	return result;
}

std::vector<unsigned char> CanMessage::GetData()
{
	// Copy data atomically
	uint64_t data = _data.load();
	// Use copied to create the vector
	std::vector<unsigned char> retval(_dataSize);
	memcpy(retval.data(), &data, retval.size());
	return retval;
}

void CanMessage::SetIsSender(bool isSender)
{
	_isSender = isSender;
}

void CanMessage::SetMsgId(uint32_t id)
{
	if (id < 2048)
	{
		_id = id;
	}
	else
	{
		_logger(fmi2Warning, "The ID is max. 11 Bit (2047). This one is: " + std::to_string(id));
		_id = 2047;
	}
}

void CanMessage::SetLoggerCallback(LoggerCallback logger)
{
	_logger = logger;
}

void CanMessage::SetFrame(int resolution, std::vector<CanValueConverter> valueConverters)
{
	// Does it fit?
	if ((valueConverters.size() * resolution + RESERVE_BITS_COUNT) > (MAX_MESSAGE_SIZE * 8))
	{
		_logger(fmi2Warning, "Too many values for a CanFrame, max. is 8 Byte");
		return;
	}
	_resolution = resolution;
	_valueConverters = valueConverters;
	// Calculate the size of the data in bytes. Ceil so everything fits.
	_dataSize = static_cast<int>(ceil(
		((float)_resolution * valueConverters.size() + RESERVE_BITS_COUNT) / 8));
}

void CanMessage::SetValue(int pos, double value)
{
	if (pos < _valueConverters.size())
	{
		// Encode only one value
		uint64_t encoded = _valueConverters[pos].EncodeValue(value);
		// Update current dataValues
		auto updateValues = getEncodedValues();
		updateValues[pos] = encoded;
		setEncodedValues(updateValues);
	}
	else
	{
		_logger(fmi2Warning, "Position of value is out of range.");
	}
}

void CanMessage::SetValues(std::vector<double> values)
{
	// Check if this value can be sent
	if (!_isSender)
	{
		_logger(fmi2Warning, "Tried to set the values of a receiver message.");
		return;
	}

	if (values.size() > _valueConverters.size())
	{
		_logger(fmi2Warning, "Trying to set more values than defined in the config.");
		return;
	}

	// Encode the values
	std::vector<uint64_t> encodedValues;
	for (auto i = 0; i < values.size(); i++)
	{
		encodedValues.push_back(_valueConverters[i].EncodeValue(values[i]));
	}
	// Set the data
	setEncodedValues(encodedValues);
}

void CanMessage::SetData(std::vector<unsigned char> data)
{
	// Copy data into primitve type
	uint64_t temp = 0;
	memcpy(&temp, data.data(), data.size());
	// Atomic assignment
	_data.store(temp);
}

std::vector<uint64_t> CanMessage::getEncodedValuesTim()
{
	// Create the result vector
	std::vector<uint64_t> result;
	// Create a 64 Bit primitive storage for bitwise operation
	uint64_t msgBits;
	memcpy(&msgBits, GetData().data(), sizeof(msgBits));
	// Mask the Bits via the resolution
	uint64_t mask = (1 << _resolution) - 1;

	// Iterate over the values count
	for (size_t i = 0; i < _valueConverters.size(); i++)
	{
		// Apply the mask and add to the result vector
		result.push_back(msgBits & mask);
		// Shift the bits to the next value
		msgBits = msgBits >> _resolution;
	}

	// Since we pushed in the first value first <<
	// We started extracting the last value >>
	std::reverse(result.begin(), result.end());
	return result;
}

std::vector<uint64_t> CanMessage::getEncodedValues()
{
	// Get the atomic data
	auto data = GetData();
	// Empty result vector
	std::vector<uint64_t> result;

	// We need to remember the current position: which Byte and the position inside that Byte
	int byteIndex = 0;
	// Reserve for timestamp
	int bitIndex = 4;

	for (int i = 0; i < _valueConverters.size(); i++)
	{
		// Generate an empty value
		uint64_t currentValue = 0;
		// We might have to read several times if the value is split into different bytes
		int bitsLeft = _resolution;
		// Read the whole value
		while (bitsLeft > 0)
		{
			// How much can we read from the current byte?
			int bits_leftInByte = 8 - bitIndex;
			// How manye bits can we write? 
			int fragmentLength = std::min(bits_leftInByte, bitsLeft);

			// Create mask
			uint64_t mask = (1 << fragmentLength) - 1;
			// Prepare the current value for OR operation
			currentValue = currentValue << fragmentLength;
			// Extract and prepare the value from the current byte
			uint64_t currentByte = data[byteIndex];
			currentByte = currentByte >> (8 - bitIndex - fragmentLength);
			currentByte &= mask;
			// Merge the value from the byte with the total value
			currentValue |= currentByte;

			// Update the left bits count
			bitsLeft -= fragmentLength;
			// Update the indices
			bitIndex += fragmentLength;
			if (bitIndex == 8)
			{
				// Next byte!
				byteIndex++;
				bitIndex = 0;
				bits_leftInByte = 8;
			}
		}
		// Add the value
		result.push_back(currentValue);
	}
	return result;
}

void CanMessage::setEncodedValuesTim(std::vector<uint64_t> encodedValues)
{
	// Create mask for the resolution
	uint64_t mask = (1 << _resolution) - 1;

	uint64_t msgBits = 0;

	// Push the first value first left <<
	for (uint64_t value : encodedValues)
	{
		// Move old values forward
		msgBits = msgBits << _resolution;
		// Mask the new value
		value = mask & value;
		// Copy the value to the msg
		msgBits = msgBits | value;
	}

	// Copy the data to a vector
	std::vector<unsigned char> buff(_dataSize);
	memcpy(buff.data(), &msgBits, buff.size());
	SetData(buff);
}

void CanMessage::setEncodedValues(std::vector<uint64_t> encodedValues)
{
	// While I look at the message as an 64 Bit Integer pushing all values << after each other
	// The binary intger bytes are [7][6]...[1][0]
	// Jonas prefers lookig at the data bytes order: [0][1][2][3]

	// Work with an vector of bytes instead of int64
	std::vector<unsigned char> data(_dataSize, 0);

	// We need to remember the current position: which Byte and the position inside that Byte
	int current_Byte = 0;
	int bit_index_inByte = 4;	//the first 4 bits are reserved
	for (uint64_t currentValue : encodedValues)
	{
		// We might have to split a value so remember the amount of remaining bits
		int bits_left = _resolution;
		while (bits_left > 0)
		{
			// Check if there is still some space left in the current byte
			int bits_leftInByte = 8 - bit_index_inByte;
			if (bits_leftInByte == 0)
			{
				// If there is no space move to thhe next byte
				current_Byte++;
				bit_index_inByte = 0;
				bits_leftInByte = 8;
			}

			// How manye bits can we write? 
			int fragmentLength = 0;
			if (bits_leftInByte >= bits_left)
			{
				fragmentLength = bits_left;
			}
			else
			{
				fragmentLength = bits_leftInByte;
			}

			// Update the current byte
			uint64_t fragment_shifted = (currentValue >> (bits_left - fragmentLength));
			uint64_t fragment_mask = ((1 << (fragmentLength)) - 1);
			uint64_t fragment = fragment_shifted&fragment_mask;
			uint64_t shift = 8 - fragmentLength - bit_index_inByte;
			data[current_Byte] |= (fragment << (shift));
			// Update the index in byte and the bits left for the current value
			bit_index_inByte += fragmentLength;
			bits_left -= bits_leftInByte;
		}
	}

	// Update the atomic data
	SetData(data);
}
