#include "CanConnectionBase.h"
#include "CanMessage.h"

void CanConnectionBase::SetReadCallback(ReadCallback callback)
{
	_readCallback = callback;
}

void CanConnectionBase::SetLoggerCallback(LoggerCallback logger)
{
	_logger = logger;
}

void CanConnectionBase::onRead(unsigned long id, std::vector<unsigned char> data)
{
	_readCallback(id, data);
}

void CanConnectionBase::onLog(fmi2Status status, std::string msg)
{
	_logger(status, msg);
}
