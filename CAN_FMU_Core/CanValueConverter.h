#pragma once
#include "TypeDefs.h"
#include <vector>

typedef std::vector<std::pair<uint64_t, double>> ScalingVector;

/// Use this class to encode or decode values
class CanValueConverter
{
public:
	CanValueConverter();
	CanValueConverter(ScalingVector scaling, LoggerCallback logger);
	~CanValueConverter();
	uint64_t EncodeValue(double value);
	double DecodeValue(uint64_t value);
	void SetScalingVector(ScalingVector scaling);
	void SetLoggerCallback(LoggerCallback logger);
private:
	// Reference points for dynamic scaling. Sorted!!!
	ScalingVector _scaling;
	// Logger for the fmi standard
	LoggerCallback _logger;
};

