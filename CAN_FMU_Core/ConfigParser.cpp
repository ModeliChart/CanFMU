#include "ConfigParser.h"
#include "CanMessage.h"
#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/json_parser.hpp"
#include <fstream>

ConfigParser::ConfigParser(LoggerCallback logger) :
	_logger(logger)
{
}

ConfigParser::~ConfigParser()
{
}

fmi2Status ConfigParser::LoadConfig(const std::string & resUri)
{
	// Create path to config file from uri
	std::string resDir(resUri);
	std::string fIdent("file:///");
	std::string fIdentShort("file:/");
	// Find the file identifier at the begin
	if (resDir.find(fIdent) == 0)
	{
		resDir = resDir.substr(fIdent.length(), resDir.length() - fIdent.length());
	}
	else if (resDir.find(fIdentShort) == 0)
	{
		resDir = resDir.substr(fIdentShort.length(), resDir.length() - fIdentShort.length());
	}
	else
	{
		// The resUri is non valid
		_logger(fmi2Error, "The fmuResourceLocation is not a valid Uri.");
		return fmi2Error;
	}

	// Try to open the config file
	std::ifstream configFile(resDir + "/Config.json");
	if (!configFile.good())
	{
		_logger(fmi2Error, "The Config.json can not be found!");
		return fmi2Error;
	}

	// Parse the json
	boost::property_tree::ptree json;
	try
	{
		boost::property_tree::read_json(configFile, json);
	}
	catch (boost::property_tree::json_parser::json_parser_error err)
	{
		_logger(fmi2Error, "Failed parsing the config: " + err.message());
		return(fmi2Error);
	}
	_guid = json.get<std::string>("GUID");
	for (auto& currentMsg : json.get_child("Messages"))
	{
		// Create a new message
		auto canMsg = std::make_shared<CanMessage>(
			_logger, currentMsg.second.get<bool>("IsSender"), currentMsg.second.get<unsigned long>("ID"));
		// Load Channels
		std::vector<CanValueConverter> conversionVector;
		for (auto& channel : currentMsg.second.get_child("Channels"))
		{
			// Scaling for each channel
			ScalingVector scale;
			for (auto& point : channel.second.get_child("ValueConversion"))
			{
				scale.push_back({ point.second.get<int>("Encoded"), point.second.get<double>("Decoded") });
			}
			conversionVector.push_back(CanValueConverter(scale, _logger));
		}
		// Set the frame layout
		canMsg->SetFrame(currentMsg.second.get<int>("Resolution"), conversionVector);
		_canMessages.push_back(canMsg);
	}
	return fmi2OK;
}

std::string ConfigParser::GetGuid()
{
	return _guid;
}

std::vector<std::shared_ptr<CanMessage>> ConfigParser::GetCanMessages()
{
	return _canMessages;
}
