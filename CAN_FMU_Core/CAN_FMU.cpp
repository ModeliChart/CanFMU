// CAN_FMU.cpp : Definiert die exportierten Funktionen f�r die DLL-Anwendung.

#ifdef WIN_PCANBASIC
#include "stdafx.h"
#endif // WIN_PCANBASIC

#include <iostream>
#include <fstream>
#include "CAN_FMU.h"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
//#include <boost/algorithm/string/split.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>

namespace fs = boost::filesystem;

CanComponent* InstantiateCan(const std::string instanceName, std::string configPath, const fmi2CallbackFunctions *functions, void(*logger) (fmi2Status status, const char *msg, CanComponent *c)) {
	// Allocate the component
	CanComponent *c = (CanComponent*)functions->allocateMemory(1, sizeof(CanComponent));
	c->Log = logger;
	c->Functions = functions;
	// Allocate the memory for the chars
	std::string name(instanceName);
	// Allocate memory and copy the name because it will go out of scop (+1 for"\0")
	c->InstanceName = (char*)c->Functions->allocateMemory(1, name.size() + 1);
	memcpy((void*)c->InstanceName, name.c_str(), name.size() + 1);

	// Init the data and info vector
	if (!fs::exists(configPath)) {
		c->Log(fmi2Error, "The Config_Can.txt has not been found.", c);
		return nullptr;
	}
	// Parse file for information and allocate the memory
	parseConfig(c, configPath);

	return c;
}
fmi2Status InitializeCan(CanComponent* c)
{
	if (c == nullptr) {
		return fmi2Error;
	}

	// Create Handles
	bool status = false;
	#ifdef WIN_PCANBASIC
	//c->ReceiveTrigger = CreateEvent(NULL, FALSE, FALSE, NULL);

	// Init the CAN
	// Initialize the CAN form the possible devices list
	
	for (TPCANHandle handle : CAN_Handles) {
		TPCANStatus result = CAN_Initialize(handle, PCAN_BAUD_1M);
		status = checkStatus(c, result);
		if (status) {
			c->Channel = handle;
			std::string msg = "Initialized CAN_Channel: ";
			msg.append(std::to_string(handle));
			c->Log(fmi2OK, msg.c_str(), c);
			break;
		}
	}
#	endif // WIN_PCANBASIC
#ifdef LINUX_SOCKETCAN
	if ((c->s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Error while opening socket");
		
	}
	else{
	strcpy(c->ifr.ifr_name, "can0");
	ioctl(c->s, SIOCGIFINDEX, &c->ifr);

	c->addr.can_family = AF_CAN;
	c->addr.can_ifindex = c->ifr.ifr_ifindex;

	c->Log(fmi2OK,"Connected to can0 interface", c);
	status = true;
	if (bind(c->s, (struct sockaddr *)&c->addr, sizeof(c->addr)) < 0) {
		perror("Error in socket bind");
		status = false;
	}
	}
#endif // LINUX_SOCKETCAN

	// Check if init worked
	if (!status) {
		c->Log(fmi2Error, "Could not connect to the CAN interface.", c);
		return fmi2Error;
	}

	// Begin the receiving
	c->IsRunning = true;
	//c->ReceiveThread = new boost::thread(&c->ReceiveThread);
	//c->ReceiveThread = CreateThread(NULL, 0, receiveEventBased, (LPVOID)c, 0, NULL);

	return fmi2OK;
}
void UninitializeCan(CanComponent* c) {
	// Memory might be cleared by the caller
	if (c != nullptr) {
		// Stop reading
		c->IsRunning = false;
		//CloseHandle(c->ReceiveTrigger);
		//CloseHandle(c->MsgMutex);
		// Free the CanBus
#ifdef WIN_PCANBASIC
		CAN_Uninitialize(c->Channel);
#endif
	}
}
void FreeCan(CanComponent* c) {
	if (c != nullptr) {
		// Free the manually allocated memory from heap
		c->Functions->freeMemory((void*)c->InstanceName);
		c->Functions->freeMemory((void*)c->GUID);
		c->Functions->freeMemory(c->Msgs);
		c->Functions->freeMemory(c->Infos);
		c->Functions->freeMemory(c);
	}
}

fmi2Status WriteValues(CanComponent *c) {
	fmi2Status fmuStatus = fmi2OK;
	if (checkStatus(c)) {
		for (int i = 0; i < c->Count; i++) {
			if (c->Infos[i].Sender) {
#ifdef WIN_PCANBASIC
bool status = checkStatus(c, CAN_Write(c->Channel, &c->Msgs[i]));
#endif // WIN_PCANBASIC
#ifdef LINUX_SOCKETCAN
bool status = true;
//Todo:Write
#endif // LINUX_SOCKETCAN

				
				if (!status) {
					fmuStatus = fmi2Warning;
				}
			}
		}
	}
	return fmuStatus;
}
void ReadValues(CanComponent *c) {
	// Keep reading until queue empty: PCAN_ERROR_QRCVEMPTY 
	//TPCANStatus status;
	// TODO use the timeStamp adequately
	//TPCANTimestamp timeStampBuffer;
	TPCANMsg msg;
#ifdef WIN_PCANBASIC
	do {
		if (c == nullptr) {
			break;
		}
		// Read data from the queue
		status = CAN_Read(c->Channel, &msg, &timeStampBuffer);

		// Break on error
		if (status == PCAN_ERROR_ILLOPERATION) {
			// An error occured -> stop reading
			checkStatus(c, status);
			break;
		}
		// Break if we did not receive a standard frame
		if (msg.MSGTYPE != PCAN_MESSAGE_STANDARD) {
			c->Log(fmi2Warning, "Received a non standard frame.", c);
			break;
		}

		// Try to obtain the mutex
		DWORD res = WaitForSingleObject(c->MsgMutex, INFINITE);
		// Assemble the message to the CanComponent
		if (res == WAIT_OBJECT_0) {
			for (int i = 0; i < c->Count; i++) {
				if (c->Infos[i].ID == msg.ID) {
					memcpy(&c->Msgs[i], &msg, sizeof(TPCANMsg));
					break;
				}
			}
		}
		// Unlock mutex
		ReleaseMutex(c->MsgMutex);
} while (status != PCAN_ERROR_QRCVEMPTY);
#endif // WIN_PCANBASIC
#ifdef LINUX_SOCKETCAN
int nbytes;
struct can_frame frame;
nbytes = read(c->s, &frame, sizeof(struct can_frame));
msg.ID = frame.can_id;
msg.LEN = frame.can_dlc;
memcpy(&msg.DATA, &frame.data, frame.can_dlc);
c->MsgMutex.lock();
	for (int i = 0; i < c->Count; i++) {
		if (c->Infos[i].ID == msg.ID) {
			memcpy(&c->Msgs[i], &msg, sizeof(TPCANMsg));
			break;
		}
	}
c->MsgMutex.unlock();


#endif // LINUX_SOCKETCAN


	
}

void parseConfig(CanComponent *c, const std::string configPath) {
	std::ifstream inputStream(fs::canonical(configPath).generic_string());
	std::string line;
	std::vector<TPCANMsg> canMsgsTemp;
	std::vector<CanFrameInfo> canInfosTemp;


	while (std::getline(inputStream, line)) {
		if (line.size() < 2) {
			// To short even for only coment
			continue;
		}

		// Check the prefix
		std::string prefix = line.substr(0, 2);

		// Comment
		if (prefix == "//") {
			continue;
		}

		// Command
		if (prefix == "$$") {
			std::vector<std::string> vecStr;
			boost::split(vecStr, line, boost::is_any_of(";"));
			// Remove the $$ from the Command
			std::string cmd = vecStr[0].substr(2, vecStr[0].length());

			// GUID
			if (cmd == "GU") {
				std::string guid = vecStr[1];
				// Allocate memory for the guid (+1 for the NULL termination)
				c->GUID = (char*)c->Functions->allocateMemory(1, guid.size() + 1);
				// Copy the guid
				memcpy((void*)c->GUID, guid.c_str(), guid.size() + 1);
				continue;
			}

			// Message
			if (cmd == "IN" || cmd == "OU") {
				// Info
				CanFrameInfo info;
				// Mode
				if (cmd == "IN") {
					info.Sender = false;
				}
				else {
					info.Sender = true;
				}
				// get the msg id
				std::size_t resLength;
				info.ID = std::stoul(vecStr[1], &resLength, 16);
				// Set to default
				for (int i = 0; i < 5; i++) {
					info.ChannelEnabled[i] = false;
					info.ChannelFactor[i] = 1;
					info.ChannelOffset[i] = 0;
				}

				// CANMsg
				TPCANMsg msg;
				msg.ID = info.ID;
				msg.LEN = 8;
#ifdef WIN_PCANBASIC
				msg.MSGTYPE = PCAN_MESSAGE_STANDARD;
#endif
				for (int i = 0; i < 8; i++) {
					msg.DATA[i] = 0;
				}

				// Append to vecs
				canMsgsTemp.push_back(msg);
				canInfosTemp.push_back(info);
				continue;
			}
		}

		// Value
		CanFrameInfo canInfo = canInfosTemp.back();
		std::vector<std::string> infoVec;
		boost::split(infoVec, line, boost::is_any_of(";"));
		// Find one that is not enabled yet
		for (int i = 0; i < 5; i++) {
			// If all channels are enabled in this frame, do not add any more
			if (!canInfo.ChannelEnabled[i]) {
				canInfo.ChannelEnabled[i] = true;
				double factor = std::stod(infoVec[0]);
				double offset = std::stod(infoVec[1]);
				canInfo.ChannelFactor[i] = factor;
				canInfo.ChannelOffset[i] = offset;
				// Do not add other infos
				break;
			}
		}
		canInfosTemp.back() = canInfo;
	}

	// Allocate global variables and copy the temporary data to the global variables
	c->Count = canInfosTemp.size();
	c->Msgs = (TPCANMsg*)c->Functions->allocateMemory(c->Count, sizeof(TPCANMsg));
	c->Infos = (CanFrameInfo*)c->Functions->allocateMemory(c->Count, sizeof(CanFrameInfo));
	// Copy data
	memcpy(c->Infos, canInfosTemp.data(), c->Count * sizeof(CanFrameInfo));
	memcpy(c->Msgs, canMsgsTemp.data(), c->Count * sizeof(TPCANMsg));
}
// Checks status based on the CAN_GetStatus function
bool checkStatus(CanComponent *c) {
#ifdef WIN_PCANBASIC
	TPCANStatus status = CAN_GetStatus(c->Channel);
	return checkStatus(c, status);
#endif
	return true;
}
#ifdef WIN_PCANBASIC
bool checkStatus(CanComponent *c, TPCANStatus status) {
	switch (status) {
	case PCAN_ERROR_BUSLIGHT:
		c->Status = fmi2Warning;
		c->Log(fmi2Warning, "ERROR: Handling a BUS-LIGHT status...", c);
		break;
	case PCAN_ERROR_BUSHEAVY:
		c->Status = fmi2Warning;
		c->Log(fmi2Warning, "ERROR: Handling a BUS-HEAVY status...", c);
		break;
	case PCAN_ERROR_BUSOFF:
		c->Status = fmi2Error;
		c->Log(fmi2Error, "ERROR: Handling a BUS-OFF status...", c);
		break;
	case PCAN_ERROR_OK:
		c->Status = fmi2OK;
		return true;
		break;
	default:
		// Ein Fehler ist aufgetreten. Die R�ckgabewert wird in Text umgewandelt und angezeigt.
		c->Status = fmi2Warning;
		char strMsg[256];
		CAN_GetErrorText(status, 0, strMsg);
		c->Log(fmi2Warning, strMsg, c);
		break;
	}
	return false;
}
#endif

void receiveEventBased(void* parameter) {
	CanComponent* c = (CanComponent*)parameter;
	// Get the trigger Handle
	//TPCANStatus status = CAN_SetValue(c->Channel, PCAN_RECEIVE_EVENT, &c->ReceiveTrigger, sizeof(c->ReceiveTrigger));

	//if (!checkStatus(c, status)) {
		// Failed!
	//	return 1;
	//}

	while (c->IsRunning)
	{
		//Wait for CAN Data and only use as much processor as neccessary
		//status = WaitForSingleObject(c->ReceiveTrigger, 1);

		ReadValues(c);

		// Signaled state :-)
		/*if (status == WAIT_OBJECT_0) {
			ReadValues(c);
		}
		else if (status == WAIT_FAILED) {
			std::string msg = std::to_string(GetLastError());
			c->Log(fmi2Warning, msg.c_str(), c);
		}*/
	}

	// Resets the Event-handle configuration, by setting a nullpointer handle
	//CAN_SetValue(c->Channel, PCAN_RECEIVE_EVENT, nullptr, sizeof(nullptr));
	//return 0;
}
