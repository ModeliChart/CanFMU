#include "CanValueConverter.h"
#include <algorithm>

CanValueConverter::CanValueConverter()
{}

CanValueConverter::CanValueConverter(ScalingVector scaling, LoggerCallback logger) :
	_scaling(scaling), _logger(logger)
{
	// Sort in ascending order
	std::sort(_scaling.begin(), _scaling.end(),
		[](std::pair<int, double> a, std::pair<int, double> b)
	{
		return a.first < b.first;
	});
}


CanValueConverter::~CanValueConverter()
{}

uint64_t CanValueConverter::EncodeValue(double value)
{
	// We can not interpolate when we have less than two points
	if (_scaling.size() < 2)
	{
		return 0;
	}

	// Find the valid scaling
	if (value < _scaling.front().second)
	{
		// Too small
		_logger(fmi2Warning, "The decoded values is smaller than the valid domain.");
		return _scaling.front().first;
	}
	else if (value > _scaling.back().second)
	{
		// Too large
		_logger(fmi2Warning, "The encoded values is larger than the valid domain.");
		return _scaling.back().first;
	}
	else
	{
		// Just right
		for (size_t i = 1; i < _scaling.size(); i++)
		{
			if (value >= _scaling[i - 1].second && value <= _scaling[i].second)
			{
				uint64_t lE = _scaling[i - 1].first;
				uint64_t uE = _scaling[i].first;
				double lD = _scaling[i - 1].second;
				double uD = _scaling[i].second;
				double factor = (uE - lE) / (uD - lD);
				// Scale!
				return static_cast<int>(lE + factor * (value - lD));
			}
		}
	}
	return 0;
}

double CanValueConverter::DecodeValue(uint64_t value)
{
	// We can not interpolate when we have less than two points
	if (_scaling.size() < 2)
	{
		return 0;
	}

	// Find the valid scaling
	if (value < _scaling.front().first)
	{
		// Too small
		_logger(fmi2Warning, "The encoded values is smaller than the valid domain.");
		return _scaling.front().second;
	}
	else if (value > _scaling.back().first)
	{
		// Too large
		_logger(fmi2Warning, "The decoded values is larger than the valid domain.");
		return _scaling.back().second;
	}
	else
	{
		for (unsigned int i = 1; i < _scaling.size(); i++)
		{
			if (value >= _scaling[i - 1].first && value <= _scaling[i].first)
			{
				uint64_t lE = _scaling[i - 1].first;
				uint64_t uE = _scaling[i].first;
				double lD = _scaling[i - 1].second;
				double uD = _scaling[i].second;
				double factor = (uD - lD) / (uE - lE);
				// Scale!
				return lD + factor * (value - lE);
			}
		}
	}
	return 0;
}

void CanValueConverter::SetScalingVector(ScalingVector scaling)
{
	_scaling = scaling;
}

void CanValueConverter::SetLoggerCallback(LoggerCallback logger)
{
	_logger = logger;
}
