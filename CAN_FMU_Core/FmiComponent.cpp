#include "FmiComponent.h"
#include <set>

FmiComponent::FmiComponent(std::string instanceName, const fmi2CallbackFunctions* callbacks) :
	_instanceName(instanceName), _callbacks(callbacks)
{
}

FmiComponent::~FmiComponent()
{
	if (_connection)
	{
		_connection->Uninitialize();
	}
}

LoggerCallback FmiComponent::GetLoggerCallback()
{
	using namespace std::placeholders;
	return std::bind(&FmiComponent::logger, this, _1, _2);
}

void FmiComponent::SetConnection(std::unique_ptr<CanConnectionBase> connection)
{
	using namespace std::placeholders;
	_connection = std::move(connection);
	_connection->SetLoggerCallback(GetLoggerCallback());
	_connection->SetReadCallback(std::bind(&FmiComponent::connection_ReadCallback, this, _1, _2));
	_connection->Initialize();
}

void FmiComponent::SetCanMessages(std::vector<std::shared_ptr<CanMessage>> canMessages)
{
	_canMessages.clear();
	for (auto msg : canMessages)
	{
		_canMessages.emplace(msg->GetId(), msg);
	}
}

fmi2Status FmiComponent::GetValues(const std::vector<fmi2ValueReference>& vr, std::vector<fmi2Real>& values)
{
	if (vr.size() != values.size())
	{
		logger(fmi2Warning, "Size of valueRefs and values do not match!");
		return fmi2Warning;
	}
	for (size_t i = 0; i < vr.size(); i++)
	{
		// Set thee value
		std::pair<int, int> valueId = DecodeVr(vr[i]);
		if (_canMessages.find(valueId.first) != _canMessages.end())
		{
			values[i] = _canMessages[valueId.first]->GetValues()[valueId.second];
		}
	}
	return fmi2OK;
}

fmi2Status FmiComponent::SetValues(const std::vector<fmi2ValueReference>& vr, const std::vector<fmi2Real>& values)
{
	if (vr.size() != values.size())
	{
		logger(fmi2Warning, "Size of valueRefs and values do not match!");
		return fmi2Warning;
	}
	// Bottleneck is the CAN BUS so send each message only once. And only send the ones that were set
	// Store the modified messages in a set
	std::set<int> modifiedMessages;
	for (int i = 0; i < vr.size(); i++)
	{
		// extract pair of msgId & channel number
		auto IDs = DecodeVr(vr[i]);
		if (_canMessages.find(IDs.first) != _canMessages.end())
		{
			// Update the messages values
			_canMessages[IDs.first]->SetValue(IDs.second, values[i]);
			// Add the messageId to the modified ones (set inserts each key only once)
			modifiedMessages.insert(IDs.first);
		}
	}
	// Send the messages
	if (_connection)
	{
		for (int msgId : modifiedMessages)
		{
			if (_canMessages.find(msgId) != _canMessages.end())
			{
				auto data = _canMessages[msgId]->GetData();
				// Only send if data is not empty to save bandwith
				if (!data.empty())
				{
					_connection->Send(msgId, data);
				}
			}
		}
	}

	return fmi2OK;
}

std::pair<int, int> FmiComponent::DecodeVr(fmi2ValueReference vr)
{
	// Message ID is everything but the last two digits.s
	int msgId = (int)(vr / 100);
	// The last two digits are the channelId.
	int channelId = vr - msgId * 100;
	return { msgId, channelId };
}

void FmiComponent::logger(fmi2Status status, std::string msg)
{
	_callbacks->logger(_callbacks->componentEnvironment, _instanceName.c_str(), status, "", msg.c_str());
}

void FmiComponent::connection_ReadCallback(unsigned long id, const std::vector<unsigned char> data)
{
	if (_canMessages.find(id) != _canMessages.end())
	{
		_canMessages[id]->SetData(data);
	}
}
