#include "CanComponent.h"
#include <string>

#ifdef WIN_PCANBASIC
// Array of possible channel handles
const TPCANHandle CAN_Handles[] = { PCAN_USBBUS1, PCAN_USBBUS2, PCAN_PCIBUS1 };
#endif // WIN_PCANBASIC

/********************************************************************
* Function declarations
********************************************************************/
// InstantiateCan, allocates the memory and loads the config
CanComponent* InstantiateCan(const std::string instanceName, const std::string configPath, const fmi2CallbackFunctions *functions, void(*logger) (fmi2Status status, const char *msg, CanComponent *c));
// Initializes the CAN and starts receiving
fmi2Status InitializeCan(CanComponent *c);
// Uninitializes the CAN Channel
void UninitializeCan(CanComponent* c);
// Frees the memory from the heap
void FreeCan(CanComponent* c);

fmi2Status WriteValues(CanComponent *c);
void ReadValues(CanComponent *c);
#ifdef WIN_PCANBASIC
DWORD WINAPI receiveEventBased(LPVOID parameter);
bool checkStatus(CanComponent *c, TPCANStatus status);
#endif // WIN_PCANBASIC
#ifdef LINUX_SOCKETCAN
void receiveEventBased(void* parameter);
#endif // LINUX_SOCKETCAN

bool checkStatus(CanComponent *c);


void parseConfig(CanComponent *c, const std::string configPath);