// Implementiert die zu exportierrenden fmi2Functions
//#include "stdafx.h"
#include "fmi2Functions.h"
#include "FmiComponent.h"
#include "ConfigParser.h"
#include "SystemFunctions.h"
#include <string>
#include <cstring>

// ValueRef: MsgId (1-4 digits) ChannelNumber (2 digits fixed)
// Example: 50103: MsgId=501 ChannelNumber=3

// Return the TypesPlatform from "fmi2TypesPlatform.h"
const char* fmi2GetTypesPlatform()
{
	return fmi2TypesPlatform;
}
// Return the Version 2.0
const char* fmi2GetVersion()
{
	return "2.0";
}


fmi2Status fmi2SetDebugLogging(fmi2Component c, fmi2Boolean loggingOn, size_t nCategories, const fmi2String categories[])
{
	return fmi2Warning;
}
fmi2Status fmi2GetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Real value[])
{
	auto comp = static_cast<FmiComponent*>(c);
	// Make some std types
	std::vector<fmi2Real> val(nvr);
	// Get the values
	fmi2Status status = comp->GetValues(std::vector<fmi2ValueReference>(vr, vr + nvr), val);
	// Update the c array
	memcpy(value, val.data(), nvr * sizeof(fmi2Real));
	return status;
}
fmi2Status fmi2GetInteger(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Integer value[])
{
	return fmi2OK;
}
fmi2Status fmi2GetBoolean(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2Boolean value[])
{
	return fmi2OK;
}
fmi2Status fmi2GetString(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, fmi2String  value[])
{
	return fmi2OK;
}

fmi2Status fmi2SetReal(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Real value[])
{
	auto comp = static_cast<FmiComponent*>(c);
	std::vector<fmi2Real> val(value, value + nvr);
	// Set the values
	return comp->SetValues(std::vector<fmi2ValueReference>(vr, vr + nvr), std::vector<fmi2Real>(value, value + nvr));
}
fmi2Status fmi2SetInteger(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Integer value[])
{
	return fmi2OK;
}
fmi2Status fmi2SetBoolean(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2Boolean value[])
{
	return fmi2OK;
}
fmi2Status fmi2SetString(fmi2Component c, const fmi2ValueReference vr[], size_t nvr, const fmi2String  value[])
{
	return fmi2OK;
}

fmi2Status fmi2GetFMUstate(fmi2Component c, fmi2FMUstate* FMUstate)
{
	return fmi2Warning;
}
fmi2Status fmi2SetFMUstate(fmi2Component c, fmi2FMUstate FMUstate)
{
	return fmi2Warning;
}
fmi2Status fmi2FreeFMUstate(fmi2Component c, fmi2FMUstate* FMUstate)
{
	return fmi2Warning;
}
fmi2Status fmi2SerializedFMUstateSize(fmi2Component c, fmi2FMUstate FMUstate, size_t *size)
{
	return fmi2Warning;
}
fmi2Status fmi2SerializeFMUstate(fmi2Component c, fmi2FMUstate FMUstate, fmi2Byte serializedState[], size_t size)
{
	return fmi2Warning;
}
fmi2Status fmi2DeSerializeFMUstate(fmi2Component c, const fmi2Byte serializedState[], size_t size, fmi2FMUstate* FMUstate)
{
	return fmi2Warning;
}

fmi2Status fmi2GetDirectionalDerivative(fmi2Component c, const fmi2ValueReference z_ref[], size_t nz,
										const fmi2ValueReference v_ref[], size_t nv, const fmi2Real dv[], fmi2Real dz[])
{
	return fmi2Warning;
}
fmi2Component fmi2Instantiate(fmi2String instanceName, fmi2Type fmuType, fmi2String fmuGUID,
							  fmi2String fmuResourceLocation, const fmi2CallbackFunctions* functions,
							  fmi2Boolean visible, fmi2Boolean loggingOn)
{
	// Create the component
	FmiComponent* c = new FmiComponent(instanceName, functions);
	auto logger = c->GetLoggerCallback();


	// Check modelType
	if (fmuType != fmi2CoSimulation)
	{
		logger(fmi2Error, "This model is CoSimulation only");
		return nullptr;
	}

	// Parse the config
	ConfigParser parser(c->GetLoggerCallback());
	if (parser.LoadConfig(fmuResourceLocation) == fmi2Error)
	{
		return nullptr;
	}

	// Check if the GUID matches
	if (parser.GetGuid().compare(fmuGUID) != 0)
	{
		logger(fmi2Error, "GUID does not match!");
		return nullptr;
	}

	// Setup the component
	c->SetConnection(GetSystemCan());
	c->SetCanMessages(parser.GetCanMessages());

	// Return the component
	return c;
	return nullptr;
}

void fmi2FreeInstance(fmi2Component c)
{
	// Delete as FmiComponent so the destructor gets called
	auto comp = static_cast<FmiComponent*>(c);
	delete(comp);
}
fmi2Status fmi2SetupExperiment(fmi2Component c, fmi2Boolean relativeToleranceDefined, fmi2Real relativeTolerance,
							   fmi2Real tStart, fmi2Boolean tStopDefined, fmi2Real tStop)
{
	// This fmu runs coninously so nothing can be set here
	return fmi2OK;
}
fmi2Status fmi2EnterInitializationMode(fmi2Component c)
{
	// Nothing to do here
	return fmi2OK;
}
fmi2Status fmi2ExitInitializationMode(fmi2Component c)
{
	//// Start the receiver Thread, from now on continously receive and send on demand
	//InitializeCan((CanComponent*)c);
	//return fmi2OK;
	return fmi2Warning;
}
fmi2Status fmi2Terminate(fmi2Component c)
{
	return fmi2Warning;
}
fmi2Status fmi2Reset(fmi2Component c)
{
	return fmi2Warning;
}


/***************************************************
Functions for FMI for Co-Simulation
****************************************************/

fmi2Status fmi2SetRealInputDerivatives(fmi2Component c, const  fmi2ValueReference vr[],
									   size_t nvr, const  fmi2Integer order[], const  fmi2Real value[])
{
	return fmi2Warning;
}
fmi2Status fmi2GetRealOutputDerivatives(fmi2Component c, const   fmi2ValueReference vr[],
										size_t  nvr, const   fmi2Integer order[], fmi2Real value[])
{
	return fmi2Warning;
}
fmi2Status fmi2DoStep(fmi2Component c, fmi2Real currentCommunicationPoint, fmi2Real
					  communicationStepSize, fmi2Boolean noSetFMUStatePriorToCurrentPoint)
{
	//// Nothing to do here, we receive event-based and the calculatins will only be made on demand (Get-Functions)
	//return ((CanComponent*)c)->Status;
	return fmi2Warning;
}
fmi2Status fmi2CancelStep(fmi2Component c)
{
	return fmi2Warning;
}
fmi2Status fmi2GetStatus(fmi2Component c, const fmi2StatusKind s, fmi2Status*  value)
{
	//CanComponent* can = (CanComponent*)c;
	//memcpy(value, &can->Status, sizeof(fmi2Status));
	//return fmi2OK;
	return fmi2Warning;
}
fmi2Status fmi2GetRealStatus(fmi2Component c, const fmi2StatusKind s, fmi2Real*    value)
{
	return fmi2Warning;
}
fmi2Status fmi2GetIntegerStatus(fmi2Component c, const fmi2StatusKind s, fmi2Integer* value)
{
	return fmi2Warning;
}
fmi2Status fmi2GetBooleanStatus(fmi2Component c, const fmi2StatusKind s, fmi2Boolean* value)
{
	return fmi2Warning;
}
fmi2Status fmi2GetStringStatus(fmi2Component c, const fmi2StatusKind s, fmi2String*  value)
{
	return fmi2Warning;
}