#pragma once
#include <memory>

// Forward declare
class CanConnectionBase;

// Get the right Connection
std::unique_ptr<CanConnectionBase> GetSystemCan();