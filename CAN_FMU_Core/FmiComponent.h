#pragma once
#include "CanMessage.h"
#include "CanConnectionBase.h"
#include <memory>
#include <map>

/// The central component that stores all necessary data and configuration.
/// Use this class to store and control the CanConnection.
class FmiComponent
{
public:
	FmiComponent(std::string instanceName, const fmi2CallbackFunctions* callbacks);
	~FmiComponent();

	LoggerCallback GetLoggerCallback();
	void SetConnection(std::unique_ptr<CanConnectionBase> connection);
	void SetCanMessages(std::vector<std::shared_ptr<CanMessage>> canMessages);

	fmi2Status GetValues(const std::vector<fmi2ValueReference> &vr, std::vector<fmi2Real> &values);
	fmi2Status SetValues(const std::vector<fmi2ValueReference> &vr, const std::vector<fmi2Real> &values);

	/// Decodes the valueRef: first= MsgId, second=ChannelNumber
	std::pair<int, int> DecodeVr(fmi2ValueReference vr);
	
private:
	// FMI stuff
	std::string _instanceName;
	const fmi2CallbackFunctions* _callbacks;
	std::unique_ptr<CanConnectionBase> _connection;
	std::map<unsigned long, std::shared_ptr<CanMessage>> _canMessages;

	// Calls the logger of the fmi2CallbackFunctions
	void logger(fmi2Status status, std::string msg);
	// Gets called when the connection read new data
	void connection_ReadCallback(unsigned long id, const std::vector<unsigned char> data);
};

