#include <stdio.h>
#include <dlfcn.h>
#include "fmi2Functions.h"
int main()
{
	void *lib_handle;
	lib_handle = dlopen("../SocketCAN_FMU/bin/x64/Debug/libSocketCAN_FMU.so", RTLD_LAZY);
	if (!lib_handle)
	{
		fprintf(stderr, "%s\n", dlerror());
		exit(1);
	}
    return 0;
}