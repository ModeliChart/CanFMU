# CanFMU
## Introduction
This FMU uses the pcan-basic lib for exchanging data via CANBUS.
FMU input variables will be sent when set via SetReal, SetInteger, etc.& output variables will be updated when received from the CANBUS.

Currently a struct CanComponent is used for storing the data for the session (old C style).
A more C++ style approach is used in the SocketFmu: https://git.rwth-aachen.de/ModeliChart/SocketFMU
## Boost
Currently boost filesystem library is used to open the config file.
The filesystem library must be built as binary (http://www.boost.org/doc/libs/1_64_0/libs/filesystem/doc/index.htm : Using the library).
The boost algorithm library is used for parsing the strings (maybe switch to json & PropertyTree).
## Wine Compability
Use WINAPI only to provide best compability with wine.
Wine compability: Compile with Visual Studio 2013 or lower. Ideal is VS2010(x86).
