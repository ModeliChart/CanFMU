#include "stdafx.h"
#include "CppUnitTest.h"
#include "FmiComponent.h"

// Yuck windows.h for debugOutput
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace NativeUnitTests
{
	// Doomed to use the C-function pointer
	void logger(fmi2ComponentEnvironment ce, fmi2String instanceName, fmi2Status status,
				fmi2String category, fmi2String message, ...)
	{
		std::string msg = instanceName;
		msg += ": ";
		msg += message;
		OutputDebugString(msg.c_str());
	}
	void stepFinished(fmi2ComponentEnvironment componentEnvironment, fmi2Status status)
	{
		// Dummy
		OutputDebugString("Step finished");
	}

	TEST_CLASS(FmiComponentTest)
	{
	public:
		/// Primary goal is to test the GetValues & SetValues methods
		TEST_METHOD(TestFmiComponent)
		{
			// Create the callbacks
			fmi2CallbackFunctions callbacks = {
			logger,
			calloc,
			free,
			stepFinished,
			this };
			// Some default values
			std::vector<fmi2ValueReference> vr = {
				0, 1, 2, 100, 101, 102, 200, 201, 202
			};
			std::vector<fmi2Real> values = {
				1, 2, 3, 4, 5, 6, 7, 8, 9
			};
			// Create the instance
			FmiComponent comp("TestInstance", &callbacks);
			// Test the VR conversion
			Assert::IsTrue(comp.DecodeVr(50103) == std::pair<int, int>(501, 3));
			Assert::IsTrue(comp.DecodeVr(42) == std::pair<int, int>(0, 42));
			Assert::IsTrue(comp.DecodeVr(424200) == std::pair<int, int>(4242, 0));
			// Create some test messages
			auto msg1 = std::make_shared<CanMessage>(comp.GetLoggerCallback(), true, 0);
			auto msg2 = std::make_shared<CanMessage>(comp.GetLoggerCallback(), true, 1);
			auto msg3 = std::make_shared<CanMessage>(comp.GetLoggerCallback(), true, 2);
			// Simple integer scaling for 4 Bit
			ScalingVector scale = { { 0,0 }, {15,15 } };
			std::vector<CanValueConverter> conv;
			for (size_t i = 0; i < values.size(); i++)
			{
				conv.push_back(CanValueConverter(scale, comp.GetLoggerCallback()));
			}
			// Setup messages for the component
			msg1->SetFrame(4, conv);
			msg2->SetFrame(4, conv);
			msg3->SetFrame(4, conv);
			comp.SetCanMessages({ msg1, msg2, msg3 });

			comp.SetValues(vr, values);
			// Read them the other way around
			std::reverse(vr.begin(), vr.end());
			std::reverse(values.begin(), values.end());
			std::vector<fmi2Real> resValues(vr.size());
			Assert::IsTrue(fmi2OK == comp.GetValues(vr, resValues));
			// Check it
			for (size_t i = 0; i < resValues.size(); i++)
			{
				Assert::IsTrue(values[i] == resValues[i]);
			}
		}
	};
}