#include "stdafx.h"
#include "CppUnitTest.h"
#include "CanMessage.h"
#include <functional>

// Yuck windows.h for debugOutput
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace NativeUnitTests
{
	TEST_CLASS(CanMessageTest)
	{
	public:

		TEST_METHOD(TestCanMessage)
		{
			using namespace std::placeholders;
			// Create a canMessage instance
			CanMessage canMsg(std::bind(&CanMessageTest::logger, this, _1, _2), false, 2048);
			// ID exceeds max (2047)
			canMsg.SetMsgId(2048);
			Assert::AreEqual((unsigned long)2047, canMsg.GetId());
			canMsg.SetMsgId(1);
			Assert::AreEqual((unsigned long)1, canMsg.GetId());

			// Test setting the values as receiver should trigger the logger
			canMsg.SetIsSender(false);
			canMsg.SetValues(std::vector<double>{ 1, 2, 3 });

			// Check encoding the Values
			std::vector<double> values{ -1, 1, 3.9, 4, 2, 5 };
			canMsg.SetIsSender(true);
			// Setup 3Bit resolution
			std::vector<CanValueConverter> conversionVector;
			ScalingVector scale = { { 0, 0 }, { 7, 4 }, { 5, 3 }, { 4, 2 } };
			for (auto value : values)
			{
				conversionVector.push_back(CanValueConverter(scale,
															 std::bind(&CanMessageTest::logger, this, _1, _2)));
			}
			canMsg.SetFrame(3, conversionVector);
			canMsg.SetValues(values);
			auto data = canMsg.GetData();
			// Check decoding the values again
			std::vector<double> expectedDecode{ 0, 1, 3.5, 4, 2, 4 };
			auto actualDecode = canMsg.GetValues();
			Assert::AreEqual(expectedDecode.size(), actualDecode.size());
			for (size_t i = 0; i < expectedDecode.size(); i++)
			{
				Assert::AreEqual(expectedDecode[i], actualDecode[i]);
			}
			// Check setting too many values (should not change the data)
			std::vector<double> tooManyValues{ -1, 1, 3.9, 4, 2, 5, 6, 7 };
			canMsg.SetValues(tooManyValues);
			auto tooManyDecode = canMsg.GetValues();
			Assert::AreEqual(expectedDecode.size(), tooManyDecode.size());
			for (size_t i = 0; i < expectedDecode.size(); i++)
			{
				Assert::AreEqual(expectedDecode[i], tooManyDecode[i]);
			}
			// Check setting single value
			canMsg.SetValue(0, 4);
			canMsg.SetValue(5, 2);
			Assert::AreEqual((double)4, canMsg.GetValue(0));
			Assert::AreEqual((double)2, canMsg.GetValue(5));
		}

	private:
		void logger(fmi2Status status, std::string msg)
		{
			// Only expect warning messages
			Assert::IsTrue(fmi2Warning == status);
			Logger::WriteMessage(msg.c_str());
		}
	};
}