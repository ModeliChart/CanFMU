#pragma once
#include "CanConnectionBase.h"
#include <atomic>
#include <thread>

class SocketCanConnection :
	public CanConnectionBase
{
public:
	SocketCanConnection();
	~SocketCanConnection();

	// Geerbt �ber CanConnectionBase
	virtual fmi2Status Initialize() override;
	virtual fmi2Status Uninitialize() override;
	virtual fmi2Status Send(unsigned long id, std::vector<unsigned char> data) override;

private:
	// The socket
	int _socketFd;
	// Thread for receiving event based
	std::thread _receiverThread;
	// Set true and stop the receiver loop
	std::atomic_bool _stop;

	// Start the receiver loop
	void startReceive();
};

