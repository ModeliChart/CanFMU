#include "SocketCanConnection.h"
#include <cstring>
#include <iostream>

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <net/if.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/time.h>

#include <linux/can.h>
#include <linux/can/raw.h>


SocketCanConnection::SocketCanConnection()
{
}

SocketCanConnection::~SocketCanConnection()
{
}

fmi2Status SocketCanConnection::Initialize()
{
	if ((_socketFd = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0)
	{
		onLog(fmi2Error, "Error while opening socket: " + std::string(strerror(errno)));
		return fmi2Error;
	}
	// Locate the CAN interface
	ifreq ifr = {};
	strcpy(ifr.ifr_name, "can0");
	if (ioctl(_socketFd, SIOCGIFINDEX, &ifr) < 0)
	{
		onLog(fmi2Error, "Locating the can0 interface failed: " + std::string(strerror(errno)));
		return fmi2Error;
	}

	// Bind the socket
	sockaddr_can addr = { AF_CAN, ifr.ifr_ifindex };
	if (bind(_socketFd, (struct sockaddr *) &addr, sizeof(addr)) < 0)
	{
		onLog(fmi2Error, "Binding the CAN-socket failed: " + std::string(strerror(errno)));
		return fmi2Error;
	}

	// Make the socket non-blocking
	int save_fd = fcntl(_socketFd, F_GETFL);
	save_fd |= O_NONBLOCK;
	fcntl(_socketFd, F_SETFL, save_fd);

	// Start receiving
	_receiverThread = std::thread(std::bind(&SocketCanConnection::startReceive, this));
}

fmi2Status SocketCanConnection::Uninitialize()
{
	// Stop the receiver thread
	std::cout << "Stopping the socket" << std::endl;
	_stop.store(true);
	std::cout << "Joining CAN receive thread" << std::endl;
	if (_receiverThread.joinable())
	{
		_receiverThread.join();
	}
	// shutdown
	shutdown(_socketFd, SHUT_WR);

	// Read remaining data from stack
	canfd_frame frame;
	std::cout << "CAN receiving remaining messages" << std::endl;
	while (read(_socketFd, &frame, sizeof(frame)) > 0)
	{
	}
	// Close the socket
	std::cout << "Closing socket" << std::endl;
	close(_socketFd);
	return fmi2OK;
}

fmi2Status SocketCanConnection::Send(unsigned long id, std::vector<unsigned char> data)
{
	// Check for gracefull shutdown
	if (!_stop.load())
	{
		can_frame frame = {};
		frame.can_id = id;
		memcpy(frame.data, data.data(), data.size());
		frame.can_dlc = data.size();

		ssize_t bytesSent = write(_socketFd, &frame, sizeof(struct can_frame));
		if (bytesSent < data.size())
		{
			onLog(fmi2Warning, "Could not send all message bytes: " + std::string(strerror(errno)));
			return fmi2Warning;
		}
	}
	return fmi2OK;
}

void SocketCanConnection::startReceive()
{
	// When starting we don't want to stop
	_stop.store(false);
	while (!_stop.load())
	{
		// Create a set of one socket
		fd_set readSet;
		FD_ZERO(&readSet);
		FD_SET(_socketFd, &readSet);
		fd_set errSet;
		FD_ZERO(&errSet);
		FD_SET(_socketFd, &errSet);
		// Create the timeval for timeout of 3 sec
		timeval timeoutTime;
		timeoutTime.tv_sec = 3;
		timeoutTime.tv_usec = 0;
		int selectRes = select(_socketFd + 1, &readSet, NULL, &errSet, &timeoutTime);
		switch (selectRes)
		{
		case -1:
			onLog(fmi2Warning, "Error on select(): " + std::string(strerror(errno)));
			return;
		case 0:
			onLog(fmi2Warning, "Timeout while receiving CAN message.");
			break;
		default:
			// Valid result returned.
			if (FD_ISSET(_socketFd, &readSet))
			{
				// Data arrived
				can_frame frame;
				int recvRes = read(_socketFd, &frame, sizeof(frame));
				std::vector<unsigned char> data(frame.can_dlc);
				memcpy(data.data(), &frame.data[0], data.size());
				onRead(frame.can_id, data);
			}
			if (FD_ISSET(_socketFd, &errSet))
			{
				// Error
				onLog(fmi2Warning, "Error in CanSocket: " + std::string(strerror(errno)));
			}
			break;
		}
	}
}
