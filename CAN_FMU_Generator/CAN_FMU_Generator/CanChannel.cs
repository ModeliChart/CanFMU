﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CAN_FMU_Generator
{
    public struct ConversionPoint
    {
        public UInt64 Encoded;
        public double Decoded;
    }

    public class CanChannel : INotifyPropertyChanged
    {
        public CanChannel()
        {
            _name = null;
            _unit = null;
            _low = 0;
            _high = 0;
        }

        private int _resolution;
        // If the resolution changes the conversion also changes so notify the channel
        public int Resolution
        {
            get => _resolution;
            set
            {
                _resolution = value;
                NotifyPropertyChanged("Resolution");
            }
        }

        private string _name;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
                NotifyPropertyChanged("Name");
            }
        }
        private string _unit;
        public string DisplayUnit
        {
            get
            {
                return _unit;
            }
            set
            {
                _unit = value;
                NotifyPropertyChanged("DisplayUnit");
            }
        }
        // Range of DisplayValue, BitRange is fixed 12Bits -> [0;4095]
        private double _low;
        public double Low
        {
            get
            {
                return _low;
            }
            set
            {
                _low = value;
                NotifyPropertyChanged("Low");
            }
        }
        private double _high;
        public double High
        {
            get
            {
                return _high;
            }
            set
            {
                _high = value;
                NotifyPropertyChanged("High");
            }
        }

        // For serialization
        public List<ConversionPoint> ValueConversion
        {
            get
            {
                return new List<ConversionPoint>() {
                    new ConversionPoint() { Encoded = 0, Decoded = Low},
                    new ConversionPoint() { Encoded = (UInt64)Math.Pow(2,_resolution) - 1, Decoded = High}
                };
            }
            set
            {
                if (value.Count > 1)
                {
                    Low = value[0].Decoded;
                    High = value[1].Decoded;
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

    }
}
