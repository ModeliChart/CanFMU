﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Xml.Serialization;
using System.Windows.Forms;
using System.ComponentModel;

namespace CAN_FMU_Generator
{
    class JsonWrapper
    {
        private string _guid;
        public string GUID
        {
            get => _guid;
            set => _guid = value;
        }

        private IEnumerable<CanMessage> _messages;
        public IEnumerable<CanMessage> Messages
        {
            get => _messages;
            set => _messages = value;
        }

    }

    class FmuHelper
    {
        /// <summary>
        /// Creates a CAN fmu with the defined messages.
        /// </summary>
        /// <param name="path">The path including the filename where the fmu will be created.</param>
        /// <param name="messages"></param>
        public static void CreateFmu(string path, IEnumerable<CanMessage> messages)
        {
            string guid = Guid.NewGuid().ToString();

            // Create folder structure
            var tempPath = Path.GetTempFileName();
            tempPath = tempPath.Replace(".tmp", "");
            string root = Path.Combine(tempPath, "CAN_FMU");
            string res = Path.Combine(root, "resources");
            string bin = Path.Combine(root, "binaries");
            string win64 = Path.Combine(bin, "win64");
            string linux64 = Path.Combine(bin, "linux64");
            string fileCAN_FMU = Path.Combine(win64, "CAN_FMU.dll");
            string filePCANBasic = Path.Combine(win64, "PCANBasic.dll");
            string fileCAN_FMU_Linux = Path.Combine(linux64, "CAN_FMU.so");
            string fileJSON = Path.Combine(res, "Config.json");
            string fileXML = Path.Combine(root, "modelDescription.xml");
            Directory.CreateDirectory(res);
            Directory.CreateDirectory(bin);
            Directory.CreateDirectory(win64);
            Directory.CreateDirectory(linux64);   // By Michelle


            // Create the xml and config file
            createXML(fileXML, guid, messages);
            createJSON(fileJSON, guid, messages);

            // Copy the binaries
            File.Copy("WindowsBinary/CAN_FMU.dll", fileCAN_FMU);
            File.Copy("PCANBasic.dll", filePCANBasic);
            File.Copy("LinuxBinary/CAN_FMU.so", fileCAN_FMU_Linux);



            // Zip it
            try
            {
                if (File.Exists(path))
                {

                    File.Delete(path);

                }
                ZipFile.CreateFromDirectory(root, path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            // Delete the temp files
            Directory.Delete(tempPath, true);
        }

        static void createJSON(string path, string guid, IEnumerable<CanMessage> messages)
        {
            var json = new JsonWrapper() { GUID = guid, Messages = messages };
            var serializer = new JsonSerializer();
            using (var writer = new StreamWriter(path, false))
            using (JsonWriter jWriter = new JsonTextWriter(writer))
            {
                jWriter.Formatting = Formatting.Indented;
                serializer.Serialize(jWriter, json);
            }
        }

        static void createXML(string path, string guid, IEnumerable<CanMessage> messages)
        {
            // Create with basic info
            fmiModelDescription fmd = new fmiModelDescription();
            fmd.modelName = "CAN_FMU";
            fmd.author = "Tim Übelhör";
            fmd.description = "FMU to send and receive CAN messages.";
            fmd.fmiVersion = "2.0";
            fmd.generationDateAndTime = DateTime.UtcNow;
            fmd.generationTool = "CAN_FMU_Generator";
            fmiModelDescriptionCoSimulation[] coSimulation = new fmiModelDescriptionCoSimulation[1];
            coSimulation[0] = new fmiModelDescriptionCoSimulation();
            coSimulation[0].modelIdentifier = "CAN_FMU";
            coSimulation[0].canHandleVariableCommunicationStepSize = true;
            coSimulation[0].needsExecutionTool = false;
            fmd.CoSimulation = coSimulation;
            fmd.guid = guid;

            // Create collection of ScalarVariables
            var variables = new List<fmi2ScalarVariable>();
            foreach (var message in messages)
            {
                // We need a counter for the channels             
                for (int c = 0; c < message.Channels.Count; c++)
                {
                    var channel = message.Channels[c];
                    // Only if the channel is configured
                    if (!string.IsNullOrEmpty(channel.Name))
                    {
                        // Real variable description
                        fmi2ScalarVariableReal realvariable = new fmi2ScalarVariableReal();
                        realvariable.unit = channel.DisplayUnit;
                        realvariable.displayUnit = channel.DisplayUnit;
                        realvariable.min = channel.Low;
                        realvariable.max = channel.High;
                        realvariable.maxSpecified = true;
                        realvariable.minSpecified = true;

                        // ScalarVariable description
                        fmi2ScalarVariable variable = new fmi2ScalarVariable();
                        variable.name = channel.Name;
                        variable.valueReference = (uint)(message.ID * 100 + c);
                        variable.description = "MsgID: " + message.ID + ", Channel number: " + c;
                        variable.initial = fmi2ScalarVariableInitial.exact;
                        if (message.IsSender)
                        {
                            // The iputs the the fmu are the outputs of the can
                            variable.causality = fmi2ScalarVariableCausality.input;
                            variable.variability = fmi2ScalarVariableVariability.tunable;
                        }
                        else
                        {
                            variable.causality = fmi2ScalarVariableCausality.output;
                            variable.variability = fmi2ScalarVariableVariability.discrete;
                        }
                        // Add variable to list
                        variable.Item = realvariable;
                        variables.Add(variable);
                    }
                }
            }
            // Apply the variables to the model
            fmiModelDescriptionModelVariables vars = new fmiModelDescriptionModelVariables();
            fmd.ModelVariables = new fmiModelDescriptionModelVariables();
            fmd.ModelVariables.ScalarVariable = variables.ToArray();
            // Serialize the xml
            using (TextWriter writer = new StreamWriter(path, false))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(fmiModelDescription));
                serializer.Serialize(writer, fmd);
            }
        }

        public static IList<CanMessage> LoadFmu(string path, out string guid)
        {
            // Unzip fmu to temp directory
            var tempPath = Path.GetTempFileName().Replace(".tmp", "");
            ZipFile.ExtractToDirectory(path, tempPath);

            // Read the json
            var serializer = new JsonSerializer();
            JsonWrapper json;
            using (var reader = new StreamReader(Path.Combine(
                new string[] { tempPath, "resources", "Config.json" })))
            using (var jReader = new JsonTextReader(reader))
            {
                json = serializer.Deserialize<JsonWrapper>(jReader);
                guid = json.GUID;

            }
            Directory.Delete(tempPath, true);
            return json.Messages.ToList();
        }
    }
}
