﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Linq;

namespace CAN_FMU_Generator
{
    public class CanMessage : INotifyPropertyChanged
    {
        // Currently 4 Bit reserve
        public static readonly int MESSAGE_SIZE = 64 - 4;

        public CanMessage() : this(500)
        {

        }
        public CanMessage(int msgId = 500)
        {
            // Do not create the List in Standard constructor because of serialization issues
            Channels = new BindingList<CanChannel>();
            ID = msgId;
            // Create some default channels
            Resolution = 10;
        }

        private int _resolution;
        public int Resolution
        {
            get => _resolution;
            set
            {
                // Set the resolution
                if (value > 0)
                {
                    _resolution = value;
                }
                else
                {
                    _resolution = 1;
                }
                // MaxChannelCount might have changed so update ChannelCount
                ChannelCount = ChannelCount;
                // Update the resolution for each channel
                foreach (var channel in _channels)
                {
                    // Each channel has to know the resolution for calculating the sclaling
                    channel.Resolution = Resolution;
                }
                NotifyPropertyChanged("Resolution");
            }
        }

        public int ChannelCount
        {
            get => _channels.Count;
            set
            {
                // Don't use more channels than possible in a Message
                int channelCount = value;
                if (channelCount > MaxChannelCount)
                {
                    channelCount = MaxChannelCount;
                }
                // Update the channels collection
                while (_channels.Count > channelCount)
                {
                    _channels.RemoveAt(_channels.Count - 1);
                }
                while (_channels.Count < channelCount)
                {
                    var channel = new CanChannel() { Resolution = this.Resolution };
                    channel.PropertyChanged += Channel_PropertyChanged;
                    _channels.Add(channel);
                    
                }
                NotifyPropertyChanged("ChannelCount");
            }
        }

        private void Channel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // A channels name must be unique
            if(e.PropertyName == "Name")
            {
                var channel = sender as CanChannel;
                // Compare to all other names
                foreach(var current in Channels)
                {
                    if(current != channel && current.Name == channel?.Name)
                    {
                        channel.Name = channel.Name + " channel name must be unique";
                    }
                }
            }
        }

        public int MaxChannelCount
        {
            get => MESSAGE_SIZE / Resolution;
        }

        private int _msgId;
        public int ID
        {
            get
            {
                return _msgId;
            }
            set
            {
                if (value < 0 || value > 2047)
                {
                    return;
                }
                _msgId = value;
                NotifyPropertyChanged("ID");
            }
        }

        private bool _sender;
        /// <summary>
        /// If true this message will be sent, otherwise received
        /// </summary>
        public bool IsSender
        {
            get
            {
                return _sender;
            }
            set
            {

                _sender = value;
                NotifyPropertyChanged("Sender");
            }
        }

        public string MsgIdHex
        {
            get
            {
                return "0x" + _msgId.ToString("X3") + " / " + _msgId;
            }
            set
            {
                int temp;
                if (value.StartsWith("0x"))
                {
                    string numbers = value.Substring(2);
                    if (int.TryParse(numbers,
                        System.Globalization.NumberStyles.HexNumber, System.Globalization.CultureInfo.InvariantCulture,
                        out temp))
                    {
                        ID = temp;
                    }
                }
                else
                {

                    if (int.TryParse(value, out temp))
                    {
                        ID = temp;
                    }
                }
            }
        }

        private BindingList<CanChannel> _channels;
        [JsonConverter(typeof(ChannelsJsonConverter))]
        public BindingList<CanChannel> Channels
        {
            get
            {
                return _channels;
            }
            set
            {
                _channels = value;
                // Update the resolution for each channel
                foreach (var channel in _channels)
                {
                    // Each channel has to know the resolution for calculating the sclaling
                    channel.Resolution = _resolution;
                }
                NotifyPropertyChanged("Channels");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
    }

    public class ChannelsJsonConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType is IEnumerable<CanChannel>;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            return serializer.Deserialize<BindingList<CanChannel>>(reader);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var channels = value as IEnumerable<CanChannel>;
            // Serialize only channels with valid name
            channels = from channel in channels
                       where !String.IsNullOrEmpty(channel.Name)
                       select channel;
            serializer.Serialize(writer, channels);
        }
    }
}
