﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;


namespace CAN_FMU_Generator
{
    public partial class MainWindow : Form
    {
        private BindingList<CanMessage> _messages;

        public MainWindow()
        {
            InitializeComponent();
            dgvChannels.AutoGenerateColumns = false;
            // Initialize DataSources & Binding
            _messages = new BindingList<CanMessage>();
            cbMsgID.DataSource = _messages;
            // Create one message on startup
            _messages.Add(new CanMessage(500));
            cbMsgID.SelectedIndex = -1;
            cbMsgID.SelectedIndex = 0;
            // Initialize temp folder
            string temp = Path.GetTempPath();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            _messages.Add(new CanMessage(500));
            cbMsgID.SelectedIndex = _messages.Count - 1;
            dgvChannels.DataSource = _messages[cbMsgID.SelectedIndex].Channels;
        }
        private void btnRemove_Click(object sender, EventArgs e)
        {
            int i = cbMsgID.SelectedIndex;
            if (i > -1)
            {
                _messages.RemoveAt(i);
            }
        }

        private void cbMsgID_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbMsgID.SelectedIndex >= 0)
            {
                var currentMessage = _messages[cbMsgID.SelectedIndex];
                dgvChannels.DataSource = currentMessage.Channels;
                chkSender.Checked = currentMessage.IsSender;
                txtID.Text = currentMessage.ID.ToString();
                txtResolution.Text = currentMessage.Resolution.ToString();
                txtChannelCount.Text = currentMessage.ChannelCount.ToString();
            }
            else
            {
                dgvChannels.DataSource = null;
            }
        }
        private void chkSender_CheckedChanged(object sender, EventArgs e)
        {
            int i = cbMsgID.SelectedIndex;
            if (i > -1)
            {
                _messages[i].IsSender = chkSender.Checked;
            }
        }

        private void btnCreateFMU_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.DefaultExt = "fmu";
            dialog.Filter = "Functional Mock-up Interface files (*.fmu) | *.fmu";
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                FmuHelper.CreateFmu(dialog.FileName, _messages);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = "fmu";
            dialog.Filter = "FMU Files (*.fmu) | *.fmu";
            dialog.RestoreDirectory = true;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                // Wild casting for BindingList
                _messages = new BindingList<CanMessage>(FmuHelper.LoadFmu(dialog.FileName, out string guid));
                if (_messages.Count > 0)
                {
                    // Refresh datagridview & comboBox
                    cbMsgID.DataSource = _messages;
                    cbMsgID.SelectedIndex = -1;
                    cbMsgID.SelectedIndex = 0;
                }
            }

        }

        private void txtID_Leave(object sender, EventArgs e)
        {
            int i = cbMsgID.SelectedIndex;
            if (i > -1)
            {
                _messages[i].MsgIdHex = txtID.Text;
            }
        }

        private void txtResolution_Leave(object sender, EventArgs e)
        {
            if (cbMsgID.SelectedIndex > -1)
            {
                var message = _messages[cbMsgID.SelectedIndex];
                if (int.TryParse(txtResolution.Text, out int resolution))
                {
                    message.Resolution = resolution;
                }
            }
        }

        private void txtChannelCount_Leave(object sender, EventArgs e)
        {
            int i = cbMsgID.SelectedIndex;
            if (i > -1 && int.TryParse(txtChannelCount.Text, out int channelCount))
            {
                _messages[i].ChannelCount = channelCount;
            }
        }

        private void txtChannelCount_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                txtChannelCount_Leave(sender, e);
            }
        }

        private void txtResolution_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                txtResolution_Leave(sender, e);
            }
        }

        private void txtID_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                txtID_Leave(sender, e);
            }
        }
    }
}
