﻿namespace CAN_FMU_Generator
{
    partial class MainWindow
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgvChannels = new System.Windows.Forms.DataGridView();
            this.ChannelName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DisplayUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Low = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.High = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbMsgID = new System.Windows.Forms.ComboBox();
            this.lblMsgId = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnRemove = new System.Windows.Forms.Button();
            this.txtID = new System.Windows.Forms.TextBox();
            this.lblEditID = new System.Windows.Forms.Label();
            this.btnCreateFMU = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.chkSender = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtResolution = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtChannelCount = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChannels)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvChannels
            // 
            this.dgvChannels.AllowUserToAddRows = false;
            this.dgvChannels.AllowUserToDeleteRows = false;
            this.dgvChannels.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvChannels.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvChannels.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvChannels.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvChannels.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ChannelName,
            this.DisplayUnit,
            this.Low,
            this.High});
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.NullValue = "null";
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvChannels.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgvChannels.Location = new System.Drawing.Point(28, 29);
            this.dgvChannels.Margin = new System.Windows.Forms.Padding(7);
            this.dgvChannels.Name = "dgvChannels";
            this.dgvChannels.Size = new System.Drawing.Size(1045, 310);
            this.dgvChannels.TabIndex = 1;
            // 
            // ChannelName
            // 
            this.ChannelName.DataPropertyName = "Name";
            this.ChannelName.HeaderText = "Name";
            this.ChannelName.Name = "ChannelName";
            // 
            // DisplayUnit
            // 
            this.DisplayUnit.DataPropertyName = "DisplayUnit";
            this.DisplayUnit.HeaderText = "DisplayUnit";
            this.DisplayUnit.Name = "DisplayUnit";
            // 
            // Low
            // 
            this.Low.DataPropertyName = "Low";
            this.Low.HeaderText = "Low";
            this.Low.Name = "Low";
            // 
            // High
            // 
            this.High.DataPropertyName = "High";
            this.High.HeaderText = "High";
            this.High.Name = "High";
            // 
            // cbMsgID
            // 
            this.cbMsgID.DisplayMember = "MsgIdHex";
            this.cbMsgID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMsgID.FormattingEnabled = true;
            this.cbMsgID.Location = new System.Drawing.Point(201, 381);
            this.cbMsgID.Margin = new System.Windows.Forms.Padding(7);
            this.cbMsgID.Name = "cbMsgID";
            this.cbMsgID.Size = new System.Drawing.Size(277, 37);
            this.cbMsgID.TabIndex = 2;
            this.cbMsgID.ValueMember = "MsgId";
            this.cbMsgID.SelectedIndexChanged += new System.EventHandler(this.cbMsgID_SelectedIndexChanged);
            // 
            // lblMsgId
            // 
            this.lblMsgId.AutoSize = true;
            this.lblMsgId.Location = new System.Drawing.Point(21, 388);
            this.lblMsgId.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblMsgId.Name = "lblMsgId";
            this.lblMsgId.Size = new System.Drawing.Size(147, 29);
            this.lblMsgId.TabIndex = 3;
            this.lblMsgId.Text = "Message ID:";
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(497, 377);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(7);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(175, 51);
            this.btnAdd.TabIndex = 4;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnRemove
            // 
            this.btnRemove.Location = new System.Drawing.Point(686, 377);
            this.btnRemove.Margin = new System.Windows.Forms.Padding(7);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(175, 51);
            this.btnRemove.TabIndex = 5;
            this.btnRemove.Text = "Remove";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(201, 442);
            this.txtID.Margin = new System.Windows.Forms.Padding(7);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(277, 35);
            this.txtID.TabIndex = 6;
            this.txtID.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtID_KeyUp);
            this.txtID.Leave += new System.EventHandler(this.txtID_Leave);
            // 
            // lblEditID
            // 
            this.lblEditID.AutoSize = true;
            this.lblEditID.Location = new System.Drawing.Point(21, 448);
            this.lblEditID.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.lblEditID.Name = "lblEditID";
            this.lblEditID.Size = new System.Drawing.Size(90, 29);
            this.lblEditID.TabIndex = 7;
            this.lblEditID.Text = "Edit ID:";
            // 
            // btnCreateFMU
            // 
            this.btnCreateFMU.Location = new System.Drawing.Point(28, 553);
            this.btnCreateFMU.Margin = new System.Windows.Forms.Padding(7);
            this.btnCreateFMU.Name = "btnCreateFMU";
            this.btnCreateFMU.Size = new System.Drawing.Size(175, 51);
            this.btnCreateFMU.TabIndex = 10;
            this.btnCreateFMU.Text = "Save FMU";
            this.btnCreateFMU.UseVisualStyleBackColor = true;
            this.btnCreateFMU.Click += new System.EventHandler(this.btnCreateFMU_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(217, 553);
            this.btnLoad.Margin = new System.Windows.Forms.Padding(7);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(175, 51);
            this.btnLoad.TabIndex = 13;
            this.btnLoad.Text = "Load FMU";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // chkSender
            // 
            this.chkSender.AutoSize = true;
            this.chkSender.Location = new System.Drawing.Point(497, 446);
            this.chkSender.Margin = new System.Windows.Forms.Padding(7);
            this.chkSender.Name = "chkSender";
            this.chkSender.Size = new System.Drawing.Size(229, 33);
            this.chkSender.TabIndex = 14;
            this.chkSender.Text = "Sender message";
            this.chkSender.UseVisualStyleBackColor = true;
            this.chkSender.CheckedChanged += new System.EventHandler(this.chkSender_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 506);
            this.label1.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(134, 29);
            this.label1.TabIndex = 16;
            this.label1.Text = "Resolution:";
            // 
            // txtResolution
            // 
            this.txtResolution.Location = new System.Drawing.Point(201, 500);
            this.txtResolution.Margin = new System.Windows.Forms.Padding(7);
            this.txtResolution.Name = "txtResolution";
            this.txtResolution.Size = new System.Drawing.Size(277, 35);
            this.txtResolution.TabIndex = 15;
            this.txtResolution.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtResolution_KeyUp);
            this.txtResolution.Leave += new System.EventHandler(this.txtResolution_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(492, 503);
            this.label2.Margin = new System.Windows.Forms.Padding(7, 0, 7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 29);
            this.label2.TabIndex = 18;
            this.label2.Text = "Channel count:";
            // 
            // txtChannelCount
            // 
            this.txtChannelCount.Location = new System.Drawing.Point(686, 500);
            this.txtChannelCount.Margin = new System.Windows.Forms.Padding(7);
            this.txtChannelCount.Name = "txtChannelCount";
            this.txtChannelCount.Size = new System.Drawing.Size(277, 35);
            this.txtChannelCount.TabIndex = 17;
            this.txtChannelCount.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtChannelCount_KeyUp);
            this.txtChannelCount.Leave += new System.EventHandler(this.txtChannelCount_Leave);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(216F, 216F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1101, 623);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtChannelCount);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtResolution);
            this.Controls.Add(this.chkSender);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnCreateFMU);
            this.Controls.Add(this.lblEditID);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lblMsgId);
            this.Controls.Add(this.cbMsgID);
            this.Controls.Add(this.dgvChannels);
            this.Margin = new System.Windows.Forms.Padding(7);
            this.Name = "MainWindow";
            this.Text = "CAN FMU Generator";
            ((System.ComponentModel.ISupportInitialize)(this.dgvChannels)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView dgvChannels;
        private System.Windows.Forms.ComboBox cbMsgID;
        private System.Windows.Forms.Label lblMsgId;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label lblEditID;
        private System.Windows.Forms.Button btnCreateFMU;
        private System.Windows.Forms.Button btnLoad;
        private System.Windows.Forms.CheckBox chkSender;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtResolution;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChannelName;
        private System.Windows.Forms.DataGridViewTextBoxColumn DisplayUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Low;
        private System.Windows.Forms.DataGridViewTextBoxColumn High;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtChannelCount;
    }
}

