// CAN_FMU_Test.cpp : Definiert den Einstiegspunkt f�r die Konsolenanwendung.
//

#include "stdafx.h"
#include "fmi2Functions.h"
#include <stdio.h>
#include <iostream>
#include <Windows.h>


int main()
{
	HINSTANCE dllHandle = NULL;
	fmi2GetVersionTYPE * getVersion;
	dllHandle = LoadLibraryA("C:\\Users\\Tim\\AppData\\Local\\Temp\\ModeliChart\\CAN_Receiver_Bit0\\binaries\\win32\\CAN_FMU.dll");
	if (NULL != dllHandle)
	{
		//Get pointer to our function using GetProcAddress:
		getVersion = (fmi2GetVersionTYPE *)GetProcAddress(dllHandle,
			"fmi2GetVersion");
		const char* res = getVersion();
		std::cout << res << std::endl;
	}
	else {
		DWORD res = GetLastError();
		std::cout << res << std::endl;
	}
	return 0;
}

